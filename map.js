

var _loadQueue = [];
var map = false;
document.addEventListener("DOMContentLoaded", function () {
    map = createMap();
    _loadQueue.forEach(function (geojson) {
        addGeoJsonData(geojson);
    });

    google.maps.event.addListener(map.data, 'click', function(e) {
        document.getElementById("footer").innerText = e.feature.getProperty("description");
    });
});


function createMap() {
    return new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(60, 6),
        zoom: 2
    });
}

function addGeoJsonData(geojson) {
    if (map) {
        map.data.addGeoJson(geojson);
        zoom();
    }
    else {
        _loadQueue.push(geojson);
    }
}

function addPointAt(coord, description) {
    var point = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": coord
        },
        "properties": {"description": description || " - "}
    };
    addGeoJsonData(point);

}

function zoom() {
    var bounds = new google.maps.LatLngBounds();
    map.data.forEach(function (feature) {
        processPoints(feature.getGeometry(), bounds.extend, bounds);
    });
    map.fitBounds(bounds);
}

function processPoints(geometry, callback, thisArg) {
    if (geometry instanceof google.maps.LatLng) {
        callback.call(thisArg, geometry);
    } else if (geometry instanceof google.maps.Data.Point) {
        callback.call(thisArg, geometry.get());
    } else {
        geometry.getArray().forEach(function (g) {
            processPoints(g, callback, thisArg);
        });
    }
}


