
/*** Example geojson ***/
var exampleGeoJson = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [102.0, 0.5]
            },
            "properties": {"description": "A Point..."}
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [102.0, 0.0], [103.0, 1.0], [104.0, 0.0], [105.0, 1.0]
                ]
            },
            "properties": {"description": "A Line..", "foo": "bar"}
        },
        {
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
                        [100.0, 1.0], [100.0, 0.0]]
                ]
            },
            "properties": {"description": "A Polygon.."}
        }
    ]
};

var singleFeatureExample = {
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [102.0, 5]
    }
};

/*** End example data ***/

//Add the geojson data to the map:
addGeoJsonData(exampleGeoJson);
//addGeoJsonData(singleFeatureExample);

// or add one Point at a time:
//var i = 1;
//setInterval(function () {
//    addPointAt([100.0, i++ * 2], "Point " + i);
//}, 3000);
//addPointAt([100.0, 0]);